from Fruchterman import *
import argparse
import os

"""
    Argumentos opcionales :

-v  Modo VERBOSE. Brinda informacion acerca del algoritmo.
-f  Modo FULLSCREEN.
-o  Modo OPTIMIZADO.
-s  Para elegir la resolucion.
-t  Para elegir la temperatura.
-c  Para elegir la constante C
-c  Para elegir la constante CC de enfriamiento.
-g  Para elegir la constante de la gravedad.
-i  Para elegir la cantidad de iteraciones.

"""

def graph_from_file(file_path):
    #Tries to open the file.
    ListVertices = []
    ListEdges = []
    with open(file_path,'r') as f:
        numbVertices = int(f.readline())
        for vertex in range(0,numbVertices):
            line = f.readline()
            ListVertices.append(line.rstrip())
        line = f.readline()
        while len(line) > 1:
            a,b = line.split()
            tupla = (a.rstrip(),b.rstrip())
            ListEdges.append(tupla)
            line = f.readline()
        tupleResult = (ListVertices, ListEdges)
        return tupleResult


if __name__ == "__main__":

    start_time = time.time()

    myParser = argparse.ArgumentParser()

    myParser.add_argument("graph")
    myParser.add_argument("-v","--verbose",help="set verbose mode",action="store_true",default=False)
    myParser.add_argument("-f","--fullscreen",help="set fullscreen mode", action="store_true")
    myParser.add_argument("-o",'--optimize',help="set optimized mode",action="store_true")
    myParser.add_argument("-s","--size",help="select resolution for the graph (default='600x600')",default="800x800")
    myParser.add_argument("-t","--temperature",help="set initial value for temperature (default=100)",default=100)
    myParser.add_argument("-cc","--coolconstant",help="set cool constant (default=0.98)",default=0.98)
    myParser.add_argument("-c","--Cconstant",help="set C constant (default=0.5)",default=0.5)
    myParser.add_argument("-g","--gravity",help="set amount for the gravity force (default=auto)",default=None)
    myParser.add_argument("-i","--iterations",help="set amount of iterations for the algorithm (default=100000000)",default=10000)

    args = myParser.parse_args()

    res_dic = {'500x500':[500,500],'600x600':[600,600],'800x800':[800,800],'1000x1000':[1000,1000]}

    # Graph from file
    graph_path = os.path.join(os.getcwd(),"graphs",args.graph)
    graph = graph_from_file(graph_path)

    # Initialization

    if args.fullscreen:
        pygame.display.init()
        INFO = pygame.display.Info()
        pygame.display.quit()
        res = [INFO.current_w,INFO.current_h]
    else:
        res = res_dic[args.size]

    if len(graph[0]) > 20:
        args.Cconstant = 0.25
        args.gravity = 2

    myFruchterman = FruchtermanOp(graph,res,args.optimize,args.temperature,args.coolconstant,args.gravity,args.verbose, args.Cconstant)

    myFruchterman.initialize_Obj()
    myRender = renderer(res_dic[args.size],args.fullscreen,args.verbose)
    myFruchterman.randomize_positions(myRender.circle_radius)
    myRender.setObjects(myFruchterman.objects)

    if(myFruchterman.OptimizeMode):
        myFruchterman.k *= 1.5
        myFruchterman.grid_initialization()
        myFruchterman.boxes_initialization()

    myRender.draw()

    # Steps
    while myFruchterman.temperature != 0 and  myFruchterman.i < int(args.iterations):
        myFruchterman.attractive_forces()
        myFruchterman.gravity_force()

        if(myFruchterman.OptimizeMode):
            myFruchterman.repulsive_forcesOp()
            myFruchterman.update_positions()
            myFruchterman.update_grid()
        else:
            myFruchterman.repulsive_forces()
            myFruchterman.update_positions()

        myRender.draw()
        myFruchterman.cool()

    if myFruchterman.verbose:
        if myFruchterman.temperature != 0:
            print "temperature is lower than 0.01"
        else:
            print "temperature decreased less than 0.01"

    print "My program took",  time.time() - start_time, "to run"


    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.KEYUP:
                run = False
