import pygame
import time

"""
--------------------------------------------------------------------------------
"""
class renderer:
    def __init__(self,size,screenMode,verbose):
        print('New renderer initialized.')
        self.screen = None
        self.screenMode = screenMode
        self.height = size[1]
        self.width = size[0]
        self.randomized = False
        self.vertex_color = (0,0,255)
        self.edge_color = (0,0,0)
        self.background_color = (255,255,255)
        self.circle_radius = 8
        self.circle_width = 8
        self.edge_width = 2
        self.objects = None
        self.i = 0
        self.verbose = verbose
        self.createLayout()

    def createLayout(self):
        if self.screen is None:
            print('New layout created.')
            pygame.display.init()
            pygame.display.set_caption('Fruchterman-Reingold')
            if self.screenMode:
                INFO = pygame.display.Info()
                self.screen = pygame.display.set_mode([INFO.current_w,INFO.current_h],pygame.FULLSCREEN)
            else:
                self.screen = pygame.display.set_mode([self.width,self.height])
            self.screen.fill(self.background_color)

    def setObjects(self,objects):
       self.objects = objects

    def draw(self):

        self.screen.fill(self.background_color)
        if self.verbose:
            print('Drawing', self.i)
        self.i += 1

        # Unpacks vertices and edges from object.
        V,E = self.objects

        # Create the list of drawn objects.
        drawn = []
        for a,b in E:
            # Unpack data
            a_posX = V[a][0][0]
            a_posY = V[a][0][1]
            b_posX = V[b][0][0]
            b_posY = V[b][0][1]

            # If neither of the vertices had been drawn, it draws eiter of them and also the corresponding edge.
            pygame.draw.circle(self.screen,self.vertex_color,[int(round(a_posX)),int(round(a_posY))],self.circle_radius,self.circle_width)
            drawn.append(a)
            pygame.draw.circle(self.screen,self.vertex_color,[int(round(b_posX)),int(round(b_posY))],self.circle_radius,self.circle_width)
            drawn.append(b)
            pygame.draw.line(self.screen,self.edge_color,[int(round(a_posX)),int(round(a_posY))],[int(round(b_posX)),int(round(b_posY))],self.edge_width)

        # Update the screen surface.
        pygame.display.flip()
