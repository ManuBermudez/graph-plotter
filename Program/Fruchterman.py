from Renderer import *
import math
import time
import random

class FruchtermanOp:

    ''' FRUCHTERMAN INITIALIZATION '''
    def __init__(self,graph,size,optimize,temp,cc,gravity,verbose, Cconstant):
        print('Fruchterman - Reingold Algorithm.')

        # Graph and objects setting.
        self.graph = graph
        self.objects = None

        # Layout size setting.
        self.width = size[0]
        self.height = size[1]
        self.margin = 20

        # Set of the verbose mode.
        self.verbose = verbose

        # Temperature setting.
        self.temperature = float(temp)

        # Set of the cooling constant.

        self.CC = float(cc)

        # Center of gravity for the graph.
        self.center_x = self.width/2
        self.center_y = self.height/2

        # Standard gravity.
        if gravity == None:
            self.mod_gf = (self.width+self.height)/4
        else:
            self.mod_gf = int(gravity)

        # Iteration counter setting
        self.i = 0

        #C constant setting
        self.c = float(Cconstant)

        self.objects = self.initialize_Obj()

        # Set the k factor
        self.k = self.c * math.sqrt((self.width * self.height)/len(self.objects[0]))

        self.OptimizeMode = optimize

        if verbose:
            print("---- 'ALGORITHM - SETTINGS' ----")
            print "Verbose: {}".format(self.verbose)
            print "Temperature: {}".format(self.temperature)
            print "Cool Constant: {}".format(self.CC)
            print "Gravity: {}".format(self.mod_gf)
            print "C constant: {}".format(self.c)
            print "Optimized mode: {}".format(self.OptimizeMode)

        ''' METHODS '''

    # Boxes inicialization
    def boxes_initialization(self):
        V = self.objects[0]
        for v in V:
            box = (int (V[v][0][0]/(2 * self.k)), int(V[v][0][1]/(2*self.k)))
            V[v] = ( V[v][0], V[v][1], box)
            self.grid[V[v][2]] += [v]

    # Grid inicialization.
    def grid_initialization(self):
        self.grid = {}
        self.setGrid()
        self.emptygrid = self.grid

    def setObjects(self,objects):
        print('Setting up objects.')
        self.objects = objects

    def setGrid(self):
        print('Setting up grid.')
        xBoxes = int(self.width/(2*self.k))
        yBoxes = int (self.height/(2*self.k))

        for i in range(xBoxes+1):
            for j in range(yBoxes+1):
                self.grid[(i,j)] = []


    def update_positions(self):
        V,E = self.objects
        maxForce = 0
        random.seed(random.random())
        for v in V:
            mod_f = math.sqrt((V[v][1][0])**2 + (V[v][1][1])**2)

            d = min(mod_f,self.temperature)

            AcumX = V[v][1][0]/mod_f*d
            AcumY = V[v][1][1]/mod_f*d

            # In each iteration we set the new maxForce
            # in order to know if the algorithm has finished
            maxForce = max(maxForce,AcumX,AcumY)

            newX = V[v][0][0] + AcumX
            newX = min( (self.width-self.margin) , max( self.margin, newX) )

            newY = V[v][0][1] + AcumY
            newY = min( (self.height-self.margin) , max( self.margin, newY) )

            V[v] = ( [newX,newY], [0,0], V[v][2])

        for u in V:
            for v in V:
                if u<v:
                    dist_uv = math.sqrt( (V[u][0][0]-V[v][0][0])**2 + (V[u][0][1]-V[v][0][1])**2 )
                    while dist_uv == 0:
                        V[u][0][0] += random.randrange(0,2)
                        V[v][0][0] -= random.randrange(0,2)
                        V[u][0][1] += random.randrange(0,2)
                        V[v][0][1] -= random.randrange(0,2)

                        dist_uv = math.sqrt( (V[u][0][0]-V[v][0][0])**2 + (V[u][0][1]-V[v][0][1])**2 )

        # Checks the finish condition
        if maxForce < 0.03:
            if self.verbose:
                print "Not moving at all"
            self.temperature = 0

        self.i += 1

    def update_grid(self):
        V,E = self.objects
        new_dict = self.emptygrid
        for v in V:
            new_box = (int (V[v][0][0]/(2 * self.k)), int(V[v][0][1]/(2*self.k)))
            new_dict[new_box] += [v]
            V[v] = ( V[v][0] , [0,0], new_box)

    def attractive_forces(self):
        V,E = self.objects
        for u,v in E:
            # Calculates the distance between u and v
                dist_uv = math.sqrt( (V[u][0][0]-V[v][0][0])**2 + (V[u][0][1]-V[v][0][1])**2 )
                if dist_uv != 0:
                    mod_fa = self.f_a(dist_uv)

                    fx = mod_fa * ( (V[u][0][0]-V[v][0][0]) / dist_uv)
                    fy = mod_fa * ( (V[u][0][1]-V[v][0][1]) / dist_uv)

                    # Set the new accumulators for U,V:
                    V[u] = ( [ V[u][0][0] , V[u][0][1] ],[ V[u][1][0] - fx , V[u][1][1] - fy ], V[u][2])
                    V[v] = ( [ V[v][0][0] , V[v][0][1] ],[ V[v][1][0] + fx , V[v][1][1] + fy ], V[v][2])

                    if self.verbose:
                        print 'Attractive forces FX = {} - FY = {} '.format(fx,fy)


    def repulsive_forces(self):

        V,E = self.objects

        for v in V:
            for u in V:
                if u != v:
                    #It calculate the distance between u-v
                    dist_uv = math.sqrt( (V[v][0][0]-V[u][0][0])**2 + (V[v][0][1]-V[u][0][1])**2 )
                    if dist_uv != 0:
                        mod_fr = self.f_r(dist_uv)

                        fx = mod_fr * ( (V[v][0][0]-V[u][0][0]) / dist_uv )
                        fy = mod_fr * ( (V[v][0][1]-V[u][0][1]) / dist_uv )
                        # Sts the new accumulators for U,V:
                        V[v] = ( V[v][0], [ V[v][1][0] + fx , V[v][1][1] + fy ], V[v][2] )
                        if self.verbose:
                            print 'Repulsive forces FX = {} - FY = {} '.format(fx,fy)


    def repulsive_forcesOp(self):
        V,E = self.objects
        for v in V:
            if self.grid[V[v][2]]:
                del self.grid[V[v][2]][0]
            x,y = V[v][2]
            for posx in range(x-1,x+2):
                for posy in range(y-1,y+2):
                    if ((posx,posy) not in self.grid):
                        pass
                    elif (self.grid[(posx,posy)] is []):
                        pass
                    else:
                        for u in self.grid[(posx,posy)]:
                            dist_uv = math.sqrt( (V[u][0][0]-V[v][0][0])**2 + (V[u][0][1]-V[v][0][1])**2 )
                            if dist_uv < 2*self.k and dist_uv != 0:

                                mod_fr = self.f_r(dist_uv)
                                fx = mod_fr * ( (V[v][0][0]-V[u][0][0]) / dist_uv)
                                fy = mod_fr * ( (V[v][0][1]-V[u][0][1]) / dist_uv)
                                # Sets the new accumulators for U,V:
                                V[u] = ( [ V[u][0][0] , V[u][0][1] ],[ V[u][1][0] - fx , V[u][1][1] - fy ] , V[u][2] )
                                V[v] = ( [ V[v][0][0] , V[v][0][1] ],[ V[v][1][0] + fx , V[v][1][1] + fy ] , V[v][2] )



    def gravity_force(self):
        V,E = self.objects
        for v in V:
            # Calculates the distance between u and v

            dist_uv = math.sqrt( (self.center_x-V[v][0][0])**2 + (self.center_y-V[v][0][1])**2 )
            if dist_uv != 0:
                fx = self.mod_gf * ((self.center_x-V[v][0][0]) / dist_uv)
                fy = self.mod_gf * ((self.center_y-V[v][0][1]) / dist_uv)

            # Set the new accumulators for v:
                V[v] = ( V[v][0], [ V[v][1][0] + fx , V[v][1][1] + fy ], V[v][2] )

                if self.verbose:
                    print 'Gravity forces FX = {} - FY = {} '.format(fx,fy)
                    pass

    def f_a(self,dist):
        return (math.pow(dist,2)) / self.k

    def f_r(self,dist):
        return (math.pow(self.k,2)) / dist

    # Objects Initialization
    def initialize_Obj(self):
        V,E = self.graph
        dict_V = {x:([0,0],[0,0],[]) for x in V}
        return (dict_V,E)

    def randomize_positions(self, circle_radius):
        V,E = self.objects
        for v in V:
            POS = [random.randint(self.margin,(self.width-self.margin)),random.randint(self.margin,(self.height-self.margin))]
            V[v] = ( POS ,[0,0], (0,0))

    def cool(self):
        self.temperature *= self.CC
