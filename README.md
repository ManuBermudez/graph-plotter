# Graph Plotter

This project consist of a Python program that plots a graph based on the Frutcherman-Reingold algorithm.
In order to ploat a given figure you must provide a .txt document. Said document must be placed on the graph folder with the following conditions:
1. The first line must be the number of vertices the figure has.
2. The next lines must be the name of the vertices, al separeted by new line.
3. Last must be the edges, which will be separeted by a new line each and each edge will be compose by name of the vertex, a white space and the name of the other vertex.

Example:  
3  
a  
b  
c  
a b  
a c  
b c  

Note: the name of the vertices can be either numbers, leters or a combination of them. The vertices names can't have white spaces. Loops on the same vertex are not consider.

When the program has finished ploting, if you want the window to close (while in the window) press the up key.

For help use the command -h in the terminal (python main.py -h).

This project was made in colaboration with Federico Stizza (user: federico.stizza).